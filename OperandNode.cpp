//
//  operandNode.cpp
//  Module8
//
//  Created by Shawn J Sustrich on 1/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//
#include <iostream>
#include <iomanip>
#include "OperandNode.h"

using namespace std;

//OperandNode::OperandNode(const double value) : value(value){
//
//}

std::string OperandNode::print() const
{
    std::string f_str = std::to_string(value);
    return f_str;

}

double OperandNode::evaluate()  {
  return value;
}

ostream& operator<<(ostream& os,  OperandNode& node) {

  double value = node.evaluate();
  std::string f_str = std::to_string(value);
  return os << f_str;
}
