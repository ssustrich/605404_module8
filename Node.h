//
//  Node.h
//  Module8
//
//  Created by Shawn J Sustrich on 1/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__Node__
#define __Module8__Node__

#include <stdio.h>
#include <map>
using namespace std;

class Node{
public:
  Node();
  virtual ~Node() { };
  virtual double evaluate() = 0;
  virtual std::string print() const = 0;
  virtual Node *clone() = 0;
private:
    //Node* parentNode;
};



#endif /* defined(__Module8__Node__) */
