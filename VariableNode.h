//
//  VariableNode.h
//  Module8
//
//  Created by Shawn J Sustrich on 2/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__VariableNode__
#define __Module8__VariableNode__

#include <stdio.h>
#include "OperandNode.h"

class VariableNode : public Node
{
public:
    //  VariableNode( const string variable);
  VariableNode( const string variable) : variable(variable) {}
  virtual ~VariableNode()
  {
    variableTable.clear();
  } // virtual destructor
  void setVariableName(std::string v);
  std::string getVariableName(){
    return variable;
  };
  void setTable(std::map<std::string, double>);
  std::map<std::string, double> getVariableTable(){
    return  variableTable;
  };
  void setValue();
  double getValue();
  virtual double evaluate()  override; // calculate earnings
  virtual std::string print() const override; // print object
  virtual VariableNode *clone() override
  {
    return new VariableNode(variable);
  }
  friend std::ostream& operator<<(std::ostream& os, VariableNode & node);
private:
  std::map<std::string, double> variableTable;
  std::string variable;
  double value;
};


#endif /* defined(__Module8__VariableNode__) */
