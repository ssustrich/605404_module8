//
//  VariableNode.cpp
//  Module8
//
//  Created by Shawn J Sustrich on 2/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//
#include <iostream>
#include <map>
#include "VariableNode.h"
using namespace std;

//VariableNode::VariableNode(const string variable){
//  this->variable = variable;
//}

std::string VariableNode::print() const
{
  return variable;
}

double VariableNode::evaluate() {
  std::map<std::string, double> variables = this->getVariableTable();
  return  variables[this->getVariableName()];
}
void VariableNode::setTable(std::map<std::string, double> variables)
{
  this->variableTable = variables;
}

ostream& operator<<(ostream& os, VariableNode& node) {
  string v = node.getVariableName();
  return os << v;
}