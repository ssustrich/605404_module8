//
//  operandNode.h
//  Module8
//
//  Created by Shawn J Sustrich on 1/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__OperandNode__
#define __Module8__OperandNode__

#include <stdio.h>
#include "Node.h"

class OperandNode : public Node
{
public:
    //OperandNode( const double value);
  OperandNode( const double value) : value(value) {}
  virtual ~OperandNode() { } // virtual destructor
  void setValue();
  double getValue();
  virtual double evaluate()  override; // calculate earnings
  virtual std::string print() const override; // print object
  virtual OperandNode *clone() override
  {
    return new OperandNode(value);
  }
  friend std::ostream& operator<<(std::ostream& os, OperandNode& node);
private:
  double value;

};



#endif /* defined(__Module8__OperandNode__) */
