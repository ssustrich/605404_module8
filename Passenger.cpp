//
//  Passenger.cpp
//  Module8
//
//  Created by Shawn J Sustrich on 16/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#include "Passenger.h"
namespace std {

Passenger::Passenger(int ef){
  this->endFloor = ef;

}

Passenger::~Passenger() {
    // TODO Auto-generated destructor stub
}

  void Passenger::DisposeObject()
  {
    delete this;
  }
  
  void Passenger::setStartFloor(int sf){
    this->startFloor = sf;
  }

  void Passenger::setEndFloor(int ef){
    this->endFloor = ef;
  }

  void Passenger::setStartTime(int time){
    this->startTime = time;
  }
  void Passenger::setEndTime(int time){
    this->endTime = time;
  }
  int Passenger::getStartTime(){
    return startTime;
  }
  int Passenger::getEndTime(){
    return endTime;
  }
}
