//
//  OperatorNode.h
//  Module8
//
//  Created by Shawn J Sustrich on 2/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__OperatorNode__
#define __Module8__OperatorNode__

#include <stdio.h>
#include "Node.h"



class OperatorNode : public Node
{
public:
  OperatorNode( char operation, Node & l,  Node & r);
  OperatorNode(char binop, Node *l, Node *r) : operation(binop), left(l), right(r) {}
  virtual ~OperatorNode() { } // virtual destructor
  char getOperation;
  void setOperation(char operation);
  Node* getLeft(){
    return left;
  };
  void setLeft(Node* left);
  Node* getRight(){
    return right;
  };
  void setRight(Node* right);
  virtual double evaluate()  override; // calculate earnings
  virtual std::string print() const override; // print object
  virtual OperatorNode *clone() override
  {

      return new OperatorNode(operation, left->clone(), right->clone());
  }
  std::string output();
  friend std::ostream& operator<<(std::ostream& os, OperatorNode & node);

private:
  Node* left;
  Node* right;
  char operation;

};
#endif /* defined(__Module8__OperatorNode__) */
