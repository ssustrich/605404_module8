//
//  Elevator.h
//  Module8
//
//  Created by Shawn J Sustrich on 16/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __Module8__Elevator__
#define __Module8__Elevator__

#include <stdio.h>
#include <iostream>
#include <queue>
#include <string>
#include <unordered_set>
#include <deque>
#include <queue>
#include <type_traits>
#include <bitset>
#include "Passenger.h"
#include "Floor.h"

namespace std {
  class Elevator {
  private:
    int floor;
    bitset<4> elevatorState;
    deque<Passenger> myPassengers;
    int destination;
    int *time;
    int fractionalPosition;
    void floors(Floor*);

  public:
    Elevator();
    virtual ~Elevator();
    int getElevatorState();
    void setElevatorState(int);
    void setFloor(int);
    int getFloor();
    void tick(int);
    void addPassanger(Passenger passenger);
    int getFractionalPosition();
    void setFractionalPosition(int tenth);
      // template<class Floor, size_t arrSize>
    void setFloorList(Floor *floors,int size);
  };

} /* namespace std */
#endif /* defined(__Module8__Elevator__) */
