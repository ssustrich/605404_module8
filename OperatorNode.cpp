//
//  OperatorNode.cpp
//  Module8
//
//  Created by Shawn J Sustrich on 2/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//
#include <iostream>
#include "OperatorNode.h"

using namespace std;

OperatorNode::OperatorNode( char operation, Node & l, Node & r){
  this->operation = operation;
  this->left = &l;
  this->right = &r;
}

std::string OperatorNode::print() const
{
  return "(" + left->print() + operation + right->print() + ")";
//  cout << operation
//  right->print();
//  cout << ")";
}

double OperatorNode::evaluate(){
  switch (operation) {
    case '+':
      return left->evaluate() + right->evaluate();
      break;
    case '*':
      return left->evaluate() * right->evaluate();
      break;
    case '-':
      return left->evaluate() - right->evaluate();
      break;
    case '/':
      return left->evaluate() / right->evaluate();
      break;
    default:
      return 0;
  }
  return left->evaluate() + right->evaluate();
}

ostream& operator<<(ostream& os, OperatorNode& node) {
    //  return os << "(" << node.getLeft() << node.operation << node.getRight() << ")";
  return os << node.print();
}