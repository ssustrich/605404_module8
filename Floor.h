//
//  Floor.h
//  elevator
//
//  Created by Shawn J Sustrich on 16/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#ifndef __elevator__Floor__
#define __elevator__Floor__

#include <stdio.h>
#include <iostream>
#include <queue>
#include <string>
#include <unordered_set>
#include <deque>
#include <queue>
#include <type_traits>
#include "Passenger.h"


namespace std {
  class Floor {
  private:
    /**
     * We'll need a queue of passengers waiting to go up
     */
      std::queue<Passenger> goingUpQueue;

    /**
     * and another one for passengers waiting to go down
     */
      std::queue<Passenger> goingDownQueue;

    /**
     * Yet another set I'm not sure we'll need
     * The set of elevators has cardinality of up to 4
     * The users waiting could find that
     * two or more elevators are going their direction
     * we'll need this so they randomly pick one. That is
     * When getting on an elevator they'll randomly chose
     * one out of the set of available, even if it's only one
     * In fact they could do the same when it's 0. They could
     * randomly try to get on to an elevator that isn't there
     * in which case they'll just stay there...
     * I may or may not model it like that
     */
//    deque<Elevator> elevators();

    int floorNumber;

  public:
    Floor();
    Floor(int);
    virtual ~Floor();
    int getFloorNUmber(){
      return  floorNumber;
    };
    void setFloorNumber(int);
    void addToQueue(Passenger passenger);
    bool hasWaitingPassengers();
  };
  
} /* namespace std */


#endif /* defined(__elevator__Floor__) */
