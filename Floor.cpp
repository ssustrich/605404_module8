//
//  Floor.cpp
//  elevator
//
//  Created by Shawn J Sustrich on 16/4/16.
//  Copyright (c) 2016 Digital Distortion. All rights reserved.
//

#include "Floor.h"

namespace std {

  Floor::Floor(){

  }

  Floor::Floor(int fn){
    this->floorNumber = fn;
      // TODO Auto-generated constructor stub

  }

  Floor::~Floor() {
      // TODO Auto-generated destructor stub
  }

  void Floor::setFloorNumber(int fn){
    this->floorNumber = fn;
  }

  void Floor::addToQueue(Passenger passenger){
    cout << "Adding passanger to a queue on floor: " << getFloorNUmber()  << endl;
    cout << "Passanger wants to go to floor: " << passenger.getEndFloor() << endl;
    if (passenger.getEndFloor() < getFloorNUmber()){
        //cout<<"Someone is waiting to go down"  << endl;
        Floor::goingDownQueue.push(passenger);
        //cout << "Now " << goingDownQueue.size() << " are waiting for a down elevator" << endl;
    }
    else{
        //cout<<"Someone is waiting to go up"  << endl;
      Floor::goingUpQueue.push(passenger);
        //cout << "Now " << goingUpQueue.size() << " are waiting for an up elevator"  << endl;
    }
  }

  bool Floor::hasWaitingPassengers(){
    if (Floor::goingDownQueue.empty() && Floor::goingUpQueue.empty()){
      return false;
    }
    return true;
  }

} /* namespace std */