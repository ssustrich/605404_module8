  //
  //  main.cpp
  //  Module8
  //
  //  Created by Shawn J Sustrich on 1/4/16.
  //  Copyright (c) 2016 Digital Distortion. All rights reserved.
  //

#include <iostream>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <string>

#include "Node.h"
#include "OperandNode.h"
#include "OperatorNode.h"
#include "VariableNode.h"
#include "Floor.h"
#include "Passenger.h"
#include "Elevator.h"


int main(int argc, const char * argv[]) {

    //    OperandNode a = OperandNode(2.3);
    //    VariableNode x = VariableNode("X");
    //    VariableNode y = VariableNode("Y");
    //    VariableNode z = VariableNode("Z");
    //
    //    OperatorNode d = OperatorNode('*', a, x);
    //    OperatorNode e = OperatorNode('-', z, x);
    //    OperatorNode f = OperatorNode('*', y, e);
    //    OperatorNode expression = OperatorNode('+', d, f);
    //
    //    std::map<string,double> variables;
    //    variables["X"]=2.0;
    //    variables["Y"]=3.0;
    //    variables["Z"]=5.0;
    //
    //
    //    //What I wanted was a way to set it at the root level and have it propogate
    //    //oh well
    //    x.setTable(variables);
    //    y.setTable(variables);
    //    z.setTable(variables);
    //
    //    //expression.print();
    //    cout << expression << " = " << expression.evaluate() << endl;
    //    OperatorNode *clonedExpression = expression.clone();
    //    cout << clonedExpression->print();


  Floor* floors= new Floor[100];
  for (int x = 1; x<= 100; x++){
    floors[x].setFloorNumber(x);
  }

  int time = 0;
  Elevator* elevators= new Elevator[4];
  deque<Passenger> passengersList;
  for (int y = 0; y< 4; y++){
    
    if (y%2 == 0){
      elevators[y].setFloor(0);
    }
    else
    {
      elevators[y].setFloor(100);
    }
    cout << "Elevator: " << y << " is starting at floor " <<  elevators[y].getFloor() << endl;
  }
    //cout << "We have a floor with number "  <<  floors[5].getFloorNUmber();

    //Passenger testGuy = Passenger(1);
    //testGuy.setStartFloor(1);
    //testGuy.setEndFloor(50);
    //floors[2].addToQueue(testGuy);
    //cout << "Test guy wants to go to " << testGuy.getEndFloor();
    //Elevator elevatorA = Elevator();
    //elevatorA.setElevatorState(1);
    // cout << "The elevator state is " << elevatorA.getElevatorState();

    //  int numLines = 0;
    //  ifstream in("Elevators.csv", ios::in);
    //  if (in.is_open())
    //  {
    //   while ( !in.eof())
    //    ++numLines;
    //  }
    //  in.close();

  ifstream elevatorEvents ("Elevators.csv", ios::in);
  if (elevatorEvents.is_open()){
    string skipFirstLine;
    getline(elevatorEvents, skipFirstLine);
      //    while (!elevatorEvents.eof()){
      //first we go through the file
      //I assume we'll run past the time of the last event
    while (time <= 15215){
      string line;
      getline(elevatorEvents, line);
        //int firstCommaPos = 0;
        //int secondCommaPos = 0;
      int firstCommaPos = line.find_first_of(',');
        //cout << "First comma pos is " << firstCommaPos;
        //cout << "Second comma pos is " << secondCommaPos;

      string startTime = line.substr(0, firstCommaPos);
        //string startFloor = line.substr(firstCommaPos+1, secondCommaPos);
        //string endFloor = line.substr(secondCommaPos+1, line.length());
        //cout << " ";
        //cout << startTime << " " << startFloor <<  " " <<  endFloor;

      line = line.substr(firstCommaPos+1, line.length());
      int secondCommaPos = line.find_first_of(',');
      string startFloorString = line.substr(0, secondCommaPos);
      string endFloorString = line.substr(secondCommaPos+1, line.length());
        //cout << startTime << ":" << startFloorString <<  "->" <<  endFloorString;


      int timeOfEvent = stoi(startTime);
      int startFloor = stoi(startFloorString);
      int endFloor = stoi(endFloorString);

      while (timeOfEvent > time)
      {
        cout << "No event at: " << time <<endl;
        time++;
      }
        //cout << "Event at: " << timeOfEvent << endl;
      floors[startFloor].addToQueue(Passenger(endFloor));
      time++;

      for (int y = 0; y< 4; y++){
          elevators[y].setFloorList(floors, 100);
          elevators[y].tick(time);
        }
    }

      //now we'll go until the queues for each floor is empty
    for (int x = 1; x<= 100; x++){
      if (floors[x].hasWaitingPassengers()){
        cout << "Passengers waiting on floor: " << x << endl;
      }
    }

  }
  return 0;
}

